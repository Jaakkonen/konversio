# Konversio - Telegram bot that converts images to pdfs

@konversiobot on Telegram.

## Usage

Send images as files to @konversiobot on Telegram and send `/pdf` to get the created PDF as a file.

## Development

You need a vercel account, a telegram bot and its token (Botfather from Telegram), and vercel cli.

Launch development environment with `vercel dev`. However as Telegram doesn't allow plain HTTP webhooks and vercel doesn't provide
HTTPS local development, create a HTTPS proxy using `ngrok http 3000`. [[1]](https://github.com/vercel/vercel/issues/2380) [[2]](https://github.com/vercel/vercel/pull/2382). Register the Telegram bot to use this webhook using `npm run set-tg-webhook` (TODO)


## Architecture

2 endpoints deployed to Vercel. 1 MongoDB collection & database on MongoDB Atlas.

1. `tg.rs` - Telegram webhook endpoint
2. `pdf.rs` - Returns PDF file from messages sent by specific user

These are deployed to [Vercel](https://vercel.com/) which provides invocation of
these files from a HTTP request to their managed service.

The Telegram bot is configured to have webhook pointing to the URL triggering `tg.rs`.
When sending a message to this bot, it checks whether the message is a file or a image.
If it is it's the file/image URL is appended to files array in an object identified by the chat id.

When the bot user eventually sends text message `/pdf`, the bot sends an file linking to URL invoking `pdf.rs`.
This link identifies the chat by a signed JWT token. On Telegram receiving this message it will fetch the URL
and store it into their CDN for the client to retrieve.

When Vercel receives the request from Telegram Servers `pdf.rs` is invoked. It validates the JWT signature and gets
the chat id from the data. The file URLs contained in the original messages are now fetched asynchronously, resized using `image-rs`, combined to a pdf using `printpdf`. This PDF is then returned as a HTTP response.

## Considerations

- Test `image-rs` performance against `libjpeg-turbo` and `mozjpeg`.
- MongoDB field-level encryption
- Deployment location (Where Telegram servers are?)
  - Vercel serverless is just AWS Lambda with fancy runtimes and wrappers.
    The free region they offer "Washington, D.C., USA (East) - iad1" is just AWS us-east-1 (North Virginia).
    Thus MongoDB Atlas AWS us-east-1 deployments should also be the fastest.

